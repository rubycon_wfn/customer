<?php

namespace WFN\Customer\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewCustomerRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $customer;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(\Customer $customer)
    {
        $this->customer = $customer;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

}

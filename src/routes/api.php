<?php

Route::prefix('api/v1')->middleware('api')->group(function() {
    Route::prefix('customer')->middleware('bearer')->group(function() {
        Route::get('detail', \WFN\Customer\Http\Controllers\Api\CustomerController::class . '@detail');
        Route::post('update', \WFN\Customer\Http\Controllers\Api\CustomerController::class . '@update');
    });

    Route::post('login', \WFN\Customer\Http\Controllers\Api\AuthController::class . '@login');
    Route::post('register', \WFN\Customer\Http\Controllers\Api\AuthController::class . '@register');
    Route::post('reset-password', \WFN\Customer\Http\Controllers\Api\AuthController::class . '@sendResetPasswordLink');
});

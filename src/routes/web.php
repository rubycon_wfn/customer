<?php

Route::get('dashboard', WFN\Customer\Http\Controllers\CustomerController::class . '@dashboard')->name('customer.account');

Route::prefix('customer')->group(function() {
    Route::post('update', WFN\Customer\Http\Controllers\CustomerController::class . '@update')->name('customer.update');
});

Route::get('login', WFN\Customer\Http\Controllers\Auth\LoginController::class . '@showLoginForm')->name('login');
Route::post('login', WFN\Customer\Http\Controllers\Auth\LoginController::class . '@login');
Route::post('logout', WFN\Customer\Http\Controllers\Auth\LoginController::class . '@logout')->name('logout');
Route::get('logout', function () {
    return redirect('account');
});
Route::get('register', WFN\Customer\Http\Controllers\Auth\RegisterController::class . '@showRegistrationForm')->name('register');
Route::post('register', WFN\Customer\Http\Controllers\Auth\RegisterController::class . '@register');
Route::get('password/reset', WFN\Customer\Http\Controllers\Auth\ForgotPasswordController::class . '@showLinkRequestForm')->name('password.request');
Route::post('password/email', WFN\Customer\Http\Controllers\Auth\ForgotPasswordController::class . '@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', WFN\Customer\Http\Controllers\Auth\ResetPasswordController::class . '@showResetForm')->name('password.reset');
Route::post('password/reset', WFN\Customer\Http\Controllers\Auth\ResetPasswordController::class . '@reset')->name('password.update');

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() {
    Route::prefix('customer')->group(function() {
        Route::get('/', \WFN\Customer\Http\Controllers\Admin\CustomerController::class . '@index')->name('admin.customer');
        Route::get('/edit/{id}', \WFN\Customer\Http\Controllers\Admin\CustomerController::class . '@edit')->name('admin.customer.edit');
        Route::get('/new', \WFN\Customer\Http\Controllers\Admin\CustomerController::class . '@new')->name('admin.customer.new');
        Route::post('/save', \WFN\Customer\Http\Controllers\Admin\CustomerController::class . '@save')->name('admin.customer.save');
        Route::get('/delete/{id}', \WFN\Customer\Http\Controllers\Admin\CustomerController::class . '@delete')->name('admin.customer.delete');
    });
});
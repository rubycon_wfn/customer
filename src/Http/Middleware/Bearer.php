<?php

namespace WFN\Customer\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;

class Bearer
{

    public function handle($request, Closure $next, $guard = null)
    {
        if(!$request->bearerToken() || !Auth::guard('api')->user()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $next($request);
    }

}

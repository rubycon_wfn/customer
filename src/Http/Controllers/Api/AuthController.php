<?php
namespace WFN\Customer\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Str;
use Customer;

class AuthController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Post(
     *     path="/login",
     *     summary="Login",
     *     tags={"Customer Authentification"},
     *     @SWG\Parameter(name="email", description="Email", required=true, type="string", in="formData"),
     *     @SWG\Parameter(name="password", description="Password", required=true, type="string", in="formData"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Customer"),
     *     ),
     *     @SWG\Response(response="401", description="Unauthorized"),
     * )
     */
    public function login(Request $request)
    {
        $request->validate([
            'email'    => 'required|string',
            'password' => 'required|string',
        ]);

        if(Auth::guard()->attempt($request->only('email', 'password'))) {
            $customer = Customer::where('email', $request->input('email'))->first();
            $customer->api_token = Str::random(60);
            $customer->save();
            return $customer;
        }

        return response()->json(['error' => 'Not authorized'], 401);
    }

    /**
     * @SWG\Post(
     *     path="/register",
     *     summary="Register",
     *     tags={"Customer Authentification"},
     *     @SWG\Parameter(name="email", description="Email", required=true, type="string", in="formData"),
     *     @SWG\Parameter(name="password", description="Password", required=true, type="string", in="formData"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Customer"),
     *     ),
     * )
     */
    public function register(Request $request)
    {
        Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:customer'],
            'password' => ['required', 'string', 'min:8'],
        ])->validate();

        $customer = Customer::create([
            'email'     => $request->input('email'),
            'password'  => Hash::make($request->input('password')),
            'api_token' => Str::random(60),
        ]);
        event(new Registered($customer));

        return $customer;
    }

    /**
     * @SWG\Post(
     *     path="/reset-password",
     *     summary="Send Reset Password Link",
     *     tags={"Customer Authentification"},
     *     @SWG\Parameter(name="email", description="Email", required=true, type="string", in="formData"),
     *     @SWG\Response(
     *         response=200,
     *         description="Email has been sent success",
     *         @SWG\Schema(type="object", @SWG\Property(property="success", type="boolean")),
     *     ),
     *     @SWG\Response(response="404", description="User can not be found"),
     * )
     */
    public function sendResetPasswordLink(Request $request)
    {
        $request->validate(['email' => 'required|email']);
        $response = Password::broker()->sendResetLink(
            $request->only('email')
        );
        return ['success' => $response == Password::RESET_LINK_SENT];
    }

}

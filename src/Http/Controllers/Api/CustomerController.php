<?php
namespace WFN\Customer\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Customer;

class CustomerController extends \Illuminate\Routing\Controller
{

    /**
     * @SWG\Get(
     *     path="/customer/detail",
     *     summary="Get customer details information",
     *     tags={"Customer"},
     *     security={{"Bearer":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Customer"),
     *     ),
     *     @SWG\Response(response="401", description="Unauthorized user"),
     * )
     */
    public function detail(Request $request)
    {
        return Auth::guard('api')->user();
    }

    /**
     * @SWG\Post(
     *     path="/customer/update",
     *     summary="Update customer details information",
     *     tags={"Customer"},
     *     security={{"Bearer":{}}},
     *     @SWG\Parameter(name="password", description="Password", required=false, type="string", in="formData"),
     *     @SWG\Parameter(name="email", description="Email", required=false, type="string", in="formData"),
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *         @SWG\Schema(ref="#/definitions/Customer"),
     *     ),
     *     @SWG\Response(response="401", description="Unauthorized user"),
     * )
     */
    public function update(Request $request)
    {
        $user = Auth::guard('api')->user();

        $this->validator($request->all())->validate();

        foreach ($request->all() as $key => $value) {
            if($key == 'password') {
                $value = Hash::make($value);
            }
            $user->setAttribute($key, $value);
        }

        $user->save();

        foreach(Customer::getAvailableRelations() as $key => $className) {
            $user->{$key}->fill($request->all())->save();
        }

        return $user;
    }

    protected function validator(array $data)
    {
        $rules = [];

        if(!empty($data['password'])) {
            $rules['password'] = 'string|min:8|confirmed';
            if(!Hash::check($data['current_password'], Auth::user()->password)) {
                $data['current_password'] = '';
                $rules['current_password'] = 'string|required';
            }
        }

        foreach(config('customerDetailFields', []) as $group => $fields) {
            foreach($fields as $field => $_data) {
                if(!empty($_data['required'])) {
                    $rules[$field] = 'required';
                }
            }
        }

        return Validator::make($data, $rules);
    }

}

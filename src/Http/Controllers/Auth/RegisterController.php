<?php

namespace WFN\Customer\Http\Controllers\Auth;

use WFN\Customer\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Customer;

class RegisterController extends Controller
{

    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->redirectTo = route(config('auth.redirect_after_login', 'customer.account'));
    }

    public function showRegistrationForm()
    {
        $viewPrefix = View::exists('customer.auth.register') ? 'customer.' : 'customer::';
        return view($viewPrefix . 'auth.register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:customer'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];

        foreach(config('customerDetailFields', []) as $group => $fields) {
            foreach($fields as $field => $_data) {
                $rules[$field] = [];

                if(!empty($_data['required']) || !empty($_data['options']['required'])) {
                    $rules[$field][] = 'required';
                }
                if($_data['type'] == 'text') {
                    $rules[$field][] = 'max:255';
                }
                if(!empty($_data['options']['validation'])) {
                    $rules[$field][] = $_data['options']['validation'];
                }
                $rules[$field] = implode('|', $rules[$field]);
            }
        }

        return Validator::make($data, $rules);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $customer = Customer::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => Str::random(60),
        ]);
        
        foreach(Customer::getAvailableRelations() as $key => $className) {
            $customer->load($key);
            $customer->{$key}->fill($data)->save();
        }

        event(new \WFN\Customer\Events\NewCustomerRegistered($customer));

        return $customer;
    }
}

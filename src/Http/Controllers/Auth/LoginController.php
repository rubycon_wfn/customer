<?php

namespace WFN\Customer\Http\Controllers\Auth;

use WFN\Customer\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\View;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->redirectTo = route(config('auth.redirect_after_login', 'customer.account'));
    }

    public function showLoginForm()
    {
        $viewPrefix = View::exists('customer.auth.login') ? 'customer.' : 'customer::';
        return view($viewPrefix . 'auth.login');
    }

    public function username()
    {
        $authConfig = config('auth');
        return !empty($authConfig['login_username']) ? $authConfig['login_username'] : 'email';
    }

}

<?php
namespace WFN\Customer\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Customer;

class CustomerController extends \WFN\Admin\Http\Controllers\Crud\Controller
{

    protected function _init(Request $request)
    {
        $this->gridBlock   = new \CustomerAdminGrid($request);
        $this->formBlock   = new \CustomerAdminForm();
        $this->entity      = new Customer();
        $this->entityTitle = 'Customer Users';
        $this->adminRoute  = 'admin.customer';
        return $this;
    }

    protected function _prepareData($data)
    {
        if(!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::_prepareData($data);
    }

    protected function _afterSave(Request $request)
    {
        foreach(Customer::getAvailableRelations() as $key => $className) {
            if(!$this->entity->{$key}) {
                continue;
            }
            $this->entity->{$key}->fill($request->all())->save();
        }
        return parent::_afterSave($request);
    }

    protected function validator(array $data)
    {
        $rules = [
            'email' => 'required|string|email|max:255' . (empty($data['id']) ? '|unique:customer' : ''),
        ];

        if(!empty($data['password'])) {
            $rules['password'] = 'string|min:8';
        }

        if(!empty($data['id'])) {
            foreach(config('customerDetailFields', []) as $group => $fields) {
                foreach($fields as $field => $_data) {
                    if(!empty($_data['options']['required'])) {
                        $rules[$field] = 'required';
                    }
                }
            }
        }

        return Validator::make($data, $rules);
    }
}

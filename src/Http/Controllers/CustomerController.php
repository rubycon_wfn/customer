<?php
namespace WFN\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Str;
use Customer;

class CustomerController extends \WFN\Customer\Http\Controllers\Controller
{

    public function dashboard()
    {
        $viewPrefix = View::exists('customer.dashboard') ? 'customer.' : 'customer::';
        return view($viewPrefix . 'dashboard');
    }

    public function update(Request $request)
    {
        try {
            $user = Auth::user();

            $this->validator($request->all(), $request->input('email') != $user->email)->validate();

            foreach($request->all() as $key => $value) {
                if(empty($value)) {
                    continue;
                }
                if($key == 'password') {
                    $value = Hash::make($value);
                }
                $user->setAttribute($key, $value);
            }

            $user->setAttribute('api_token', Str::random(60))->save();

            foreach(Customer::getAvailableRelations() as $key => $className) {
                $user->{$key}->fill($request->all())->save();
            }

            return back()->with([
                'success' => true,
                'message' => 'Personal data has been updated'
            ]);
        } catch (ValidationException $e) {
            $errors = [];
            foreach($e->errors() as $messages) {
                foreach ($messages as $message) {
                    $errors[] = $message;
                }
            }
            return back()->with([
                'success' => false,
                'message' => implode(' ', $errors)
            ]);
        } catch (\Exception $e) {
            return back()->with([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    protected function validator(array $data, $validateEmail = false)
    {
        $rules = [];

        if(!empty($data['password'])) {
            $rules['password'] = 'string|min:8|confirmed';
            $rules['current_password'] = function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                    return $fail(__('The current password is incorrect.'));
                }
            };
        }

        if($validateEmail) {
            $rules['email'] = 'email|max:255|unique:customer';
        }

        foreach(config('customerDetailFields', []) as $group => $fields) {
            foreach($fields as $field => $_data) {
                $rules[$field] = [];

                if(!empty($_data['required'])) {
                    $rules[$field][] = 'required';
                }
                if($_data['type'] == 'text') {
                    $rules[$field][] = 'max:255';
                }
                $rules[$field] = implode('|', $rules[$field]);
            }
        }

        return Validator::make($data, $rules);
    }

}

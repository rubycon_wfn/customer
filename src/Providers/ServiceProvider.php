<?php
namespace WFN\Customer\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;

class ServiceProvider extends WFNServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('Customer', \WFN\Customer\Model\Customer::class);
            $loader->alias('CustomerAdminGrid', \WFN\Customer\Block\Admin\Grid::class);
            $loader->alias('CustomerAdminForm', \WFN\Customer\Block\Admin\Form::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        Route::middleware('web')->group($basePath . '/routes/web.php');
        Route::middleware('api')->group($basePath . '/routes/api.php');
        
        $this->loadViewsFrom($basePath . '/views', 'customer');
        $this->publishes([
            $basePath . '/views' => resource_path('views/customer'),
        ], 'view');

        $this->loadMigrationsFrom($basePath . '/database/migrations');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
        $this->mergeConfigFrom($basePath . '/Config/auth.php', 'auth');
        $this->mergeConfigFrom($basePath . '/Config/customerDetailFields.php', 'customerDetailFields');

        $router->aliasMiddleware('auth', \WFN\Customer\Http\Middleware\Authenticate::class);
        $router->aliasMiddleware('guest', \WFN\Customer\Http\Middleware\RedirectIfAuthenticated::class);
        $router->aliasMiddleware('bearer', \WFN\Customer\Http\Middleware\Bearer::class);
    }

}

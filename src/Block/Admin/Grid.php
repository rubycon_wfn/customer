<?php
namespace WFN\Customer\Block\Admin;

use Customer;

class Grid extends \WFN\Admin\Block\Widget\AbstractGrid
{

    protected $filterableFields = ['email'];

    protected $addtitionalFilterableFields = [];

    protected $adminRoute = 'admin.customer';

    public function getInstance()
    {
        return new Customer();
    }

    protected function _beforeRender()
    {
        $this->addColumn('id', 'ID', 'text', true);
        $this->addColumn('email', 'Email');

        foreach(Customer::getAvailableRelations() as $key => $className) {
            $relation = new $className();
            foreach(config('customerDetailFields', []) as $group => $fields) {
                foreach($fields as $field => $data) {
                    if(($relation->isFillable($field)  || $this->getInstance()->isFillable($field)) && !empty($data['grid'])) {
                        $source = false;
                        if($data['type'] == 'select') {
                            $source = $data['options']['source'];
                        }
                        $this->addColumn($field, $data['label'], $data['type'], false, $source, optional($data)['options']);
                        $this->filterableFields[] = $field;
                        $this->addtitionalFilterableFields[] = $field;
                    }
                }
            }
        }

        return parent::_beforeRender();
    }

    protected function _getCollection()
    {
        $query = $this->getInstance()->newQuery();

        foreach($this->filterableFields as $field) {
            if(isset($this->request[$field]) && $this->request[$field] !== '') {
                if(in_array($field, $this->addtitionalFilterableFields)) {
                    $value = $this->request[$field];
                    $query->whereHas('detail', function($query) use ($value, $field) {
                        $query->where($field, 'like', '%' . $value . '%');
                    });
                } else {
                    $query->where($field, 'like', '%' . $this->request[$field] . '%');
                }
            }
        }

        $query->orderBy($this->getOrderBy(), $this->getDirection());

        return $query;
    }

    public function getTitle()
    {
        return 'Customers List';
    }

}
<?php
namespace WFN\Customer\Block\Admin;

use Customer;

class Form extends \WFN\Admin\Block\Widget\AbstractForm
{

    protected $adminRoute = 'admin.customer';

    protected function _beforeRender()
    {
        $this->addField('general', 'id', 'ID', 'hidden', ['required' => false]);
        $this->addField('general', 'email', 'Email', 'email', ['required' => true]);
        $this->addField('general', 'password', 'Password', 'password', ['required' => false]);

        foreach(Customer::getAvailableRelations() as $key => $className) {
            $relation = $this->getInstance()->$key ?: new $className();
            foreach(config('customerDetailFields', []) as $group => $fields) {
                foreach($fields as $field => $data) {
                    $options = (!empty($data['options']) && is_array($data['options'])) ? $data['options'] : [];
                    if($relation->isFillable($field) || $this->getInstance()->isFillable($field)) {

                        if($data['type'] == 'image' || $data['type'] == 'file') {
                            $options['publicValue'] = $this->getInstance()->getAttributeUrl($field);
                        }

                        if($data['type'] == 'date' && $this->getInstance()->getAttribute($field)) {
                            $options['fomat'] = !empty($data['options']['format']) ? $data['options']['format'] : 'Y-m-d';
                            $options['value'] = $this->getInstance()->getAttribute($field)->format($options['fomat']);

                        }

                        $this->addField($group, $field, $data['label'], $data['type'], $options);
                    } elseif(!in_array($data['type'], $this->_standardFields)) {
                        $this->addField($group, $field, $data['label'], $data['type'], $options);
                    }
                }
            }
        }

        return parent::_beforeRender();
    }

}
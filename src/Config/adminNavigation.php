<?php
return [
    'customer' => [
        'route' => 'admin.customer',
        'label' => 'Customers',
        'icon'  => 'icon-user',
    ],
];
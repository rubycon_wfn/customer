<?php
namespace WFN\Customer\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * @SWG\Definition(
 *  definition="Customer",
 *  @SWG\Property(property="id", type="integer"),
 *  @SWG\Property(property="email", type="string"),
 *  @SWG\Property(property="api_token", type="string"),
 *  @SWG\Property(property="created_at", type="string"),
 *  @SWG\Property(property="updated_at", type="string"),
 *  @SWG\Property(property="detail", type="object", @SWG\Property(property="customer_id", type="integer")),
 * )
 */
class Customer extends Authenticatable
{
    use Notifiable;

    protected $table = 'customer';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'email', 'password', 'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getAvailableRelations()
    {
        return [
            'detail' => \WFN\Customer\Model\Customer\Detail::class,
        ];
    }

    public function setAttribute($key, $value)
    {
        if(!in_array($key, $this->fillable)) {
            return $this;
        }
        return parent::setAttribute($key, $value);
    }

    public function detail()
    {
        return $this->hasOne(\WFN\Customer\Model\Customer\Detail::class);
    }

    public function getAttribute($key)
    {
        $value = parent::getAttribute($key);
        if(is_null($value)) {
            $value = $this->_getFromRelation($key);
        }
        return $value;
    }

    public function getAttributeUrl($key)
    {
        return $this->_getFromRelation($key, 'getAttributeUrl');
    }

    public function getAttributeText($key)
    {
        return $this->_getFromRelation($key, 'getAttributeText');
    }

    protected function _getFromRelation($key, $callback = 'getAttribute')
    {
        foreach(static::getAvailableRelations() as $relation => $class) {
            if(empty($this->{$relation})) {
                return false;
            }
            if(!method_exists($this->{$relation}, $callback)) {
                return false;
            }
            if($value = $this->{$relation}->{$callback}($key)) {
                return $value;
            }
        }
        return false;
    }

    public function save(array $options = [])
    {
        parent::save($options);
        foreach(static::getAvailableRelations() as $key => $class) {
            if(!$this->{$key}) {
                $this->_createRelation($key);
                $this->load($key);
            }
        }
        return $this;
    }

    protected function _createRelation($relation)
    {
        return $this->{$relation}()->create();
    }

}

<?php

namespace WFN\Customer\Model\Customer;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Storage;

class Detail extends Model
{

    const MEDIA_PATH = 'customer' . DIRECTORY_SEPARATOR;

    protected $table = 'customer_details';

    protected $fillable = [];

    protected $mediaFields = [];

    protected $dates = ['created_at', 'updated_at'];

    protected $dateFormats = [];

    protected $serviceColumns = ['id', 'customer_id', 'created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        $columns = \DB::select('DESCRIBE ' . $this->table);
        foreach($columns as $column) {
            if(!in_array($column->Field, $this->serviceColumns)) {
                $this->fillable[] = $column->Field;
            }
        }

        foreach (config('customerDetailFields', []) as $group => $fields) {
            foreach ($fields as $field => $data) {
                if($data['type'] == 'date') {
                    $this->dates[] = $field;
                    if(!empty($data['options']['format'])) {
                        $this->dateFormats[$field] = $data['options']['format'];
                    }
                }
                if($data['type'] == 'image' || $data['type'] == 'file') {
                    $this->mediaFields[] = $field;
                }
            }
        }

        return parent::__construct($attributes);
    }

    public function customer()
    {
        return $this->belongsTo(\Customer::class);
    }

    public function isFillable($field)
    {
        return in_array($field, $this->fillable);
    }

    public function fill(array $attributes)
    {
        foreach($attributes as $key => $value) {
            if(!in_array($key, $this->fillable)) {
                continue;
            }
            if(in_array($key, $this->mediaFields) && !empty($value['file']) && $value['file'] instanceof \Illuminate\Http\UploadedFile) {
                $value = $this->_uploadFile($value['file']);
                $attributes[$key] = $value;
            }
            if(in_array($key, $this->dates) && !empty($this->dateFormats[$key])) {
                $value = Carbon::createFromFormat($this->dateFormats[$key], $value);
                $attributes[$key] = $value;
            }
        }
        return parent::fill($attributes);
    }

    public function getAttributeUrl($key)
    {
        $value = $this->getAttribute($key);
        return $value ? Storage::url(static::MEDIA_PATH . $value) : false;
    }

    protected function _uploadFile($file)
    {
        $path = 'public' . DIRECTORY_SEPARATOR . static::MEDIA_PATH;
        $fileName = $file->getClientOriginalName();
        $path .= substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR;
        if(Storage::disk('local')->exists($path . $fileName)) {
            $iterator = 1;
            while(Storage::disk('local')->exists($path . $fileName)) {
                $fileName = str_replace(
                    '.' . $file->getClientOriginalExtension(),
                    $iterator++ . '.' . $file->getClientOriginalExtension(),
                    $file->getClientOriginalName()
                );
            }
        }

        $file->storeAs($path, $fileName);
        return substr($fileName, 0, 1) . DIRECTORY_SEPARATOR . substr($fileName, 1, 1) . DIRECTORY_SEPARATOR . $fileName;
    }

}